from django.apps import AppConfig


class AppReportConfig(AppConfig):
    name = 'App_Report'
