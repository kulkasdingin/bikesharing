from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

# Create your views here.

def list_report(request):
    # get items in a list here
    # if 'no_ktp' not in request.session:
    #     return render(request, "landing/landing_page.html", {
    #         "login_form": LoginForm(),
    #         "feedback" : "You have to log in first"
    #     })
    # else:
    #     if user is admin:
    #         user_type = 'ADMIN'
    #     else:
    #         user_type = 'OFFICER'
# Data dummy
    items = [
        {'reportID' : '1010', 'memberID' : '001 - Nunung', 'datetime' : '2018-11-05 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Barbie', 'datetime' : '2018-11-06 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Ken', 'datetime' : '2018-11-07 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-08 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-09 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-25 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-15 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-28 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-01 23:59:00', 'denda' : '50500', 'status': 'Available'},
        {'reportID' : '1010', 'memberID' : '001 - Dummy', 'datetime' : '2018-11-05 23:59:00', 'denda' : '50500', 'status': 'Available'},
    ]    
    user_type = "ADMIN"
    return render(request, "report.html", {
        "items" : items,
        "type" : user_type
    })