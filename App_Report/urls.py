from django.urls import path, include
from .views import list_report

app_name = 'report_page'

urlpatterns = [
    path('', list_report, name='list_report')
]