from django.shortcuts import render, redirect
from .forms import *
# Create your views here.

def voucher(req):
    return render(req, 'voucher.html')

def peminjaman(req):
    return render(req, 'peminjaman.html')

def addvoucher(req):
    form = AddVoucherForm()
    if req.method == 'POST':
        return redirect('/voucher/')
    return render(req, 'addvoucher.html', {'form' : form})

def updatevoucher(req):
    form = UpdateVoucherForm()
    if req.method == 'POST':
        return redirect('/voucher/')
    return render(req, 'updatevoucher.html', {'form' : form})
    

def createpeminjaman(req):
    form = CreatePeminjamanForm()
    if req.method == 'POST':
        return redirect('/peminjaman/')
    return render(req, 'createpeminjaman.html', {'form' : form})