from django import forms

class AddVoucherForm(forms.Form):
    nama = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Nama",
        "required" : True
    }))
    kategori = forms.CharField(label = '',  max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Kategori",
        "required" : True
    }))
    nilai_poin = forms.CharField(label = '', max_length = 20, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Nilai Poin",
        "required" : True
    }))
    deskripsi = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Deskripsi",
        "required" : True
    }))
    jumlah = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Jumlah",
        "required" : True
    }))

class UpdateVoucherForm(forms.Form):
    nama = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Nama",
        "required" : True
    }))
    kategori = forms.CharField(label = '',  max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Kategori",
        "required" : True
    }))
    nilai_poin = forms.CharField(label = '', max_length = 20, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Nilai Poin",
        "required" : True
    }))
    deskripsi = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Deskripsi",
        "required" : True
    }))
PILIHAN_STASIUN = [
    ('1', 'Stasiun Basdat Depok 1'),
    ('2', 'Stasiun Basdat Depok 2'),
    ('3', 'Stasiun Basdat Depok 3'),
    ('4', 'Stasiun Basdat Depok 4'),
    ('5', 'Stasiun Basdat Depok 5'),
    ('6', 'Stasiun Basdat Depok 6'),
    ('7', 'Stasiun Basdat Depok 7'),
    ('8', 'Stasiun Basdat Depok 8')
]
class CreatePeminjamanForm(forms.Form):
    Sepeda_stasiun = forms.CharField(label = '', max_length = 30, widget=forms.Select(choices=PILIHAN_STASIUN))