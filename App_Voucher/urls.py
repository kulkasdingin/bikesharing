from django.urls import path, include
from .views import *

app_name = 'App_Voucher'

urlpatterns = [
    path('voucher/', voucher, name='voucher'),
    path('peminjaman/', peminjaman, name='peminjaman'),
    path('addvoucher/', addvoucher, name='addvoucher'),
    path('updatevoucher/', updatevoucher, name='updatevoucher'),
    path('createpeminjaman/', createpeminjaman, name='createpeminjaman')
]