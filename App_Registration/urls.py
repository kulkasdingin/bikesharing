from django.urls import path, include
from .views import index

app_name = 'regist_page'

urlpatterns = [
    path('', index, name='index')
]