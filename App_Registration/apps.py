from django.apps import AppConfig


class AppRegistrationConfig(AppConfig):
    name = 'App_Registration'
