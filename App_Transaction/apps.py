from django.apps import AppConfig


class AppTransactionConfig(AppConfig):
    name = 'App_Transaction'
