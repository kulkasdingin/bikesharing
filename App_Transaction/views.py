from django.shortcuts import render

# Create your views here.

def list_transaction(request):
    # get items in a list here
    # if 'no_ktp' not in request.session:
    #     return render(request, "landing/landing_page.html", {
    #         "login_form": LoginForm(),
    #         "feedback" : "You have to log in first"
    #     })
    # else:
    #     if user is admin:
    #         user_type = 'ADMIN'
    #     else:
    #         user_type = 'MEMBER'
# Data dummy
    items = [
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi A', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi B', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi C', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi D', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi E', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi F', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi G', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi H', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi I', 'nominal' : '50500'},
        {'datetime' : '2018-11-05 23:59:00', 'jenis' : 'Transaksi J', 'nominal' : '50500'},
    ]    
    user_type = "MEMBER"
    return render(request, "transaction.html", {
        "items" : items,
        "type" : user_type
    })