from django.urls import path, include
from .views import list_transaction

app_name = 'transaction_page'

urlpatterns = [
    path('', list_transaction, name='list_transaction')
]