from django.shortcuts import render, redirect
from .forms import *

def stasiun(request):
    return render(request,'stasiun.html')

def create(request):
    form = createStation()
    if request.method == 'POST':
        return redirect('/station/')
    return render(request, 'create.html', {'form' : form})

def update(request):
    form = updateStation()
    if request.method == 'POST':
        return redirect('/station/')
    return render(request, 'update.html', {'form' : form})

