from django import forms

class createStation(forms.Form):
    Nama = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Nama",
        "required" : True
    }))
    Alamat = forms.CharField(label = '',  max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Alamat",
        "required" : True
    }))
    Latitude = forms.CharField(label = '', max_length = 20, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Latitude",
        "required" : True
    }))
    Longitude = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Longitude",
        "required" : True
    }))

class updateStation(forms.Form):
    Nama = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Nama",
        "required" : True
    }))
    Alamat = forms.CharField(label = '',  max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Alamat",
        "required" : True
    }))
    Latitude = forms.CharField(label = '', max_length = 20, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Latitude",
        "required" : True
    }))
    Longitude = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Longitude",
        "required" : True
    }))