from django.urls import path, include
from .views import *

app_name = 'App_Stasiun'

urlpatterns = [
    path('station/', stasiun, name='stasiun'),
    path('station/create/', create, name='create'),
    path('station/update/', update, name='update'),
]