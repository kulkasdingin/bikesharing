from django.urls import path, include
from .views import index

app_name = 'login_page'

urlpatterns = [
    path('', index, name='index')
]
