from django.urls import path, include
from .views import *

urlpatterns = [
    path('penugasan/', penugasan, name='penugasan'),
    path('penugasan/add', penugasan_add, name='penugasan_add')
]