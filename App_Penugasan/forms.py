from django import forms

class PenugasanForm(forms.Form):
    Petugas = forms.CharField(label="Petugas")
    mulai = forms.DateField(
        required=True,
        label="Tanggal Mulai (yyyy-mm-dd)",
        input_formats=['%Y-%m-%d'],
        widget=forms.DateTimeInput(attrs={'class': 'datepicker', }))
    selesai = forms.DateField(
        required=True,
        label="Tanggal Selesai (yyyy-mm-dd)",
        input_formats=['%Y-%m-%d'],
        widget=forms.DateTimeInput(attrs={'class': 'datepicker', }))
    Stasiun = forms.CharField(label="Stasiun")