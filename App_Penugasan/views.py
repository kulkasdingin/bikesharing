from django.shortcuts import render
from .forms import PenugasanForm

# Create your views here.
def penugasan(request):
    return render(request, 'penugasan.html')

def penugasan_add(request):
    response = {}
    response["form"] = PenugasanForm
    if request.method == "POST":
        form_result = PenugasanForm(request.POST)
        if form_result.is_valid():
            pass
        return HttpResponseRedirect("penugasan")
    return render(request, 'penugasan_add.html', response)
