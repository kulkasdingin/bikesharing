from django.urls import path, include
from .views import *

app_name = 'App_Sepeda'

urlpatterns = [
    path('bike/', sepeda, name='bike'),
    path('bike/create/', create, name='create'),
    path('bike/update/', update, name='update'),
]