from django.shortcuts import render,redirect
from .forms import *

def sepeda(request):
    return render(request,'sepeda.html')

def create(request):
    form = createBike()
    if request.method == 'POST':
        return redirect('/bike/')
    return render(request, 'createBike.html', {'form' : form})

def update(request):
    form = updateBike()
    if request.method == 'POST':
        return redirect('/bike/')
    return render(request, 'updateBike.html', {'form' : form})
# Create your views here.
