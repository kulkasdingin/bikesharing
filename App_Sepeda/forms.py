from django import forms

class createBike(forms.Form):
    Merk = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Merk",
        "required" : True
    }))
    Jenis = forms.CharField(label = '',  max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Jenis",
        "required" : True
    }))
    Status = forms.CharField(label = '', max_length = 20, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Status",
        "required" : True
    }))
    Stasiun = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Stasiun",
        "required" : True
    }))
    Penyumbang = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Penyumbang",
        "required" : True
    }))

class updateBike(forms.Form):
    Merk = forms.CharField(label = '', max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Merk",
        "required" : True
    }))
    Jenis = forms.CharField(label = '',  max_length = 30, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Jenis",
        "required" : True
    }))
    Status = forms.CharField(label = '', max_length = 20, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Status",
        "required" : True
    }))
    Stasiun = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Stasiun",
        "required" : True
    }))
    Penyumbang = forms.CharField(label = '', max_length = 300, widget=forms.TextInput(attrs={
        "class" : "fields",
        "placeholder" : "Penyumbang",
        "required" : True
    }))