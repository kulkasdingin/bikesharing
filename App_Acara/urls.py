from django.urls import path, include
from .views import *

urlpatterns = [
    path('acara/', acara, name='acara'),
    path('acara/add', acara_add, name='acara_add')
]