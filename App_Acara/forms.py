from django import forms

class AcaraForm(forms.Form):
    judul = forms.CharField(label="Judul", max_length = 100, required=True)
    deskripsi = forms.CharField(label="Deskripsi")
    isGratis = forms.CharField(label="Biaya")
    mulai = forms.DateField(
        required=True,
        label="Tanggal Mulai (yyyy-mm-dd)",
        input_formats=['%Y-%m-%d'],
        widget=forms.DateTimeInput(attrs={'class': 'datepicker', }))
    selesai = forms.DateField(
        required=True,
        label="Tanggal Selesai (yyyy-mm-dd)",
        input_formats=['%Y-%m-%d'],
        widget=forms.DateTimeInput(attrs={'class': 'datepicker', }))
    Stasiun = forms.CharField(label="Stasiun")
