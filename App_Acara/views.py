from django.shortcuts import render
from .forms import AcaraForm

# Create your views here.

def acara(request):
    return render(request, 'acara.html')

def acara_add(request):
    response = {}
    response["form"] = AcaraForm
    if request.method == "POST":
        form_result = AcaraForm(request.POST)
        if form_result.is_valid():
            pass
        return HttpResponseRedirect("acara")
    return render(request, 'acara_add.html', response)