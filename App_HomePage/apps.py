from django.apps import AppConfig


class AppHomepageConfig(AppConfig):
    name = 'App_HomePage'
