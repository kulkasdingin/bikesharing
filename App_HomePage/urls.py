from django.urls import path, include
from .views import index

app_name = 'App_HomePage'

urlpatterns = [
    path('', index, name='index')
]
